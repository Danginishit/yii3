<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use Yii;
use backend\models\Film;


$this->title='Film';
$this->params['breadcrumbs'][]=$this->title;
?>
<div class="Film-index">
    <h1><?php echo Html::encode($this->title);     ?></h1>

    

        <?php
     
     echo GridView::widget([
         'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
             'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                
                
                'title',
                'description',
                'release_year',
               'length',
               'rating',
               
               [
                   'attribute'=>'searchactor',
                    'label'=>'Name of Actors',
                  
                   'content'=>function($data){
                       
                        return (Film::getactorsfullnames($data->actor,count($data->actor)));
                    }
                ],

                [
                    'attribute'=>'searchcategory',
                    'label'=>'Category Name',
                    'content'=>function($data){
                        return $data->category->categoryname->name;
                    }
                ]
            ],
        ]);
        
        
        
        
        ?>
</div>