<?php

use yii\helpers\Html;
$this->title='create Actor';


$this->params['breadcrumbs'][] = ['label' => 'actor', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="actor-create">
    <h1><?php  echo Html::encode($this->title);  ?></h1>

    <?php echo $this->render('_form',[
        'model'=>$model,
    ]); 



    ?>

</div>