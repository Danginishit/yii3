<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use Yii;

$this->title='Actor';
$this->params['breadcrumbs'][]=$this->title;

?>

<div class="Actor-index">

    <h1><?php echo Html::encode($this->title);     ?></h1>
    <p>
    <?php echo Html::a('Create Actor',['create'],['class'=>'btn btn-success']);     ?>
    </p>
    <?php
       
        echo GridView::widget([
            'dataProvider'=>$dataProvider,
             'filterModel'=>$searchModel,
             'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
    
                
                'first_name',
                'last_name',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'actor_id' => $model->actor_id]);
                     }
                ],
            ],
        ]);




    ?>
</div>