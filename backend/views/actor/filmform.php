<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="filmform-form">
    <?php $form=ActiveForm::begin() ?>

    <?=  $form->field($model,'filmname');  ?>
    <?=  $form->field($model,'actor_firstname');  ?>
    <?=  $form->field($model,'actor_lastname');  ?>
    <?=  $form->field($model,'discription');  ?>

    <div class="form-group">
        <?=  Html::submitButton('save',['class'=>'btn btn-success']); ?>
    </div>
    <?php  ActiveForm::end(); ?>
</div>