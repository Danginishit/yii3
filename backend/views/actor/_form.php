<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
<div class="actor-form">
    <?php  $form=ActiveForm::begin(); ?>

    <?= $form->field($model,'first_name'); ?>
    <?= $form->field($model,'last_name'); ?>

    <div class="form-group">
        <?= Html::submitButton('save',['class'=>'btn btn-success']); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
