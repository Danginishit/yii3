<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$model=$model->getModels();
// $this->title=
?>

<?= 
DetailView::widget([
    'model' => $model,
    'attributes' => [
        [
            'attribute'=>'name',
            'value'=>function($data){
               
              return $data[0]->first_name.' '.$data[0]->last_name;
            }
        ],
        [
            'attribute'=>'email',
            'value'=>function($data){
                return $data[0]->email;
                
            }
        ],
        [
            'attribute'=>'phone',
            'value'=>function($data){
                return $data[0]->address->phone;
               
            }
        ],
        [
            'attribute'=>'address',
            'value'=>function($data){
                return $data[0]->address->address;
            }
        ],
        [
            'attribute'=>'district and postal code',
            'value'=>function($data){
                // echo '<pre>'; print_r($data[0]);exit();  
                return $data[0]->address->district.' '.$data[0]->address->postal_code;
            }
        ],
        [
            'attribute'=>'City and country',
            'value'=>function($data){
                return $data[0]->address->city->city.' '.$data[0]->address->city->country->country;
            }
        ]
        
    ],
]) 
?>