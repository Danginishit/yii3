<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Address;
use backend\models\City;
use backend\models\Country;
use yii\helpers\ArrayHelper;

?>
<div class="customer-form">
    <?php $form=ActiveForm::begin(); ?>

    <?= $form->field($model,'first_name') ?>
    <?= $form->field($model,'last_name') ?>
    <?= $form->field($model,'email') ?>
    <?= $form->field($model,'newaddress') ?>
    <?= $form->field($model,'newdistrict') ?>
    <?= $form->field($model,'postalcode') ?>
    <?= $form->field($model,'newphone') ?>
    <?= $form->field($model,'newcity')->dropDownList(Arrayhelper::map(City::find()->all(),'city_id','city'),['prompt'=>'Select City']); ?>
   <?= $form->field($model,'newcountry')->textInput(['disabled' => 'disabled']) ?>


    <div class="form-group">
        <?= Html::submitButton('save',['class'=>'btn btn-success']);  ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function(){
       $('#customer-newcity').on('change',function(){
        $.post('http://localhost/yii3/learning/advanced/backend/web/customer/getcountry',
            {
                city_id: $('#customer-newcity').val(),
            },
            function(data,status){
                $('#customer-newcountry').val(data);
                console.log(data);
            }
        )
       })
    })
</script>