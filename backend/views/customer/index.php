<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;
$this->title="Customer";
$this->params['breadcrumbs'][]=$this->title;
?>
<div class="Customer-index">
    <h1><?= Html::encode($this->title);   ?></h1>
    <p><?= Html::a('Create Customer',['create'],['class'=>'btn btn-success']) ?></p>
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'columns'=>[
            ['class'=>'yii\grid\SerialColumn'],
            [
                'attribute'=>'name',
                'content'=>function($data){
                    //echo '<pre>'; print_r($data);exit();
                    return $data->first_name.' '.$data->last_name;
                   
                }
            ],
            'email',
            [
                'attribute'=>'phone',
                'content'=>function($data){
                    return $data->address->phone;
                }
            ],
            [
                'attribute'=>'address',
                'content'=>function($data){
                    return $data->address->address;
                }
            ],
            [
                'attribute'=>'dpcode',
                'label'=>'District and Postal Code',
                'content'=>function($data){
                    return $data->address->district.' '.$data->address->postal_code;
                }
            ],
            [
                'attribute'=>'citycountry',
                'label'=>'City and County',
                'content'=>function($data){
                    return $data->address->city->city.', '.$data->address->city->country->country;
                }
            ],
            [
                'class'=>ActionColumn::className(),
                'urlCreator'=>function($action,$model){
                    return Url::toRoute([$action, 'customer_id'=>$model->customer_id,'address_id'=>$model->address_id]);
                }
            ],
        ]
        ]);
        Pjax::end();

?>
</div>