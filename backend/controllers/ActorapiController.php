<?php

namespace backend\controllers;

use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;

class ActorapiController extends ActiveController{
    public $modelClass='backend\models\Actor';

    public function actions(){
        return ArrayHelper::merge(parent::actions(),[
            'index'=>[
                'pagination'=>false,
            ],
           
        ]);
    }
}


?>