<?php

namespace backend\controllers;

use backend\models\FilmForm;
use backend\models\Actor;
use backend\models\ActorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use Yii;
use yii\helpers\Url;



class ActorController extends Controller{


    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }
    
    public function actions(){
        return [
            'error'=>[
                'class'=>'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex(){

        $searchModel= new ActorSearch();
        $dataProvider=$searchModel->search($this->request->queryParams);
        
        return $this->render('index',[
            'searchModel'=>$searchModel,
            'dataProvider'=>$dataProvider,
        ]);
    }
    public function actionView($actor_id){
       
        return $this->render('view', [
            'model' => $this->findModel($actor_id),
        ]);
    }

    public function actionCreate(){
        $model=new Actor();

        if($this->request->isPost){
            if($model->load($this->request->post())){
                if($model->save()){
                    $model->actor_id=$model->oldAttributes['actor_id'];
                     return $this->redirect(['actor/view','actor_id'=>$model->actor_id]);
                }
            }
        }
        return $this->render('create',[
            'model'=>$model,
        ]);
        
    }

    public function actionUpdate($actor_id){
        
        $model=$this->findModel($actor_id);
        if($this->request->isPost && $model->load($this->request->post()) && $model->save()){
            return $this->redirect(['view','actor_id'=>$model->actor_id]);
        }
        return $this->render('update',[
            'model'=>$model,
        ]);
    }
    public function actionFilm(){
        $model=new FilmForm();
        if($this->request->isPost){
            echo '<pre>'; print_r($this->request->post());exit();
        }
        return $this->render('filmform',['model'=>$model]);
    }


    protected function findModel($actor_id)
    {
        if (($model = Actor::findOne(['actor_id' => $actor_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}



?>