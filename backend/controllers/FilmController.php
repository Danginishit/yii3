<?php
namespace backend\controllers;

use yii\web\Controller;
use backend\models\Film;
use backend\models\FilmSearch;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class FilmController extends Controller{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actions(){
        return [
            'error'=>[
                'class'=>'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex(){
        $searchModel= new FilmSearch();
        // echo '<pre>'; print_r($searchModel);exit();
        $dataProvider=$searchModel->search($this->request->queryParams);
        // echo '<pre>'; print_r($dataProvider->getModels());exit();
        return $this->render('index',[
            'searchModel'=>$searchModel,
            'dataProvider'=>$dataProvider,
        ]);
    }

}


?>