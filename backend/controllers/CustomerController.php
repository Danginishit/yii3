<?php
namespace backend\controllers;
use backend\models\Customer;
use backend\models\CustomerSearch;
use yii\web\Controller;
use backend\models\City;
use backend\models\Country;
use backend\models\Address;

class CustomerController extends Controller{

    public function actionIndex(){
       $model=new CustomerSearch();
       $dataProvider=$model->search($this->request->queryParams);

       return $this->render('index',[
        'searchModel'=>$model,
        'dataProvider'=>$dataProvider,
       ]);
    }
    public function actionView($customer_id){
        $model=$this->findModel($customer_id);
        //echo '<pre>'; print_r($model);exit();
        return $this->render('view',[
            'model'=>$model,
            
        ]);
    }
    
    public function actionDelete($customer_id,$address_id){
        
        $model=Customer::findOne($customer_id)->delete();
        $addressmodel=Address::findOne($address_id)->delete();
       
        
        return $this->redirect(['index']);
    }

    public function actionUpdate($customer_id){
        $model=Customer::findOne($customer_id);
        $addressmodel=new Address();
        if($this->request->isPost){
            if($model->load($this->request->post())){
                $addressmodel->address=$model->newaddress;
                $addressmodel->district=$model->newdistrict;
                $addressmodel->postal_code=$model->postalcode;
                $addressmodel->phone=$model->newphone;
                $addressmodel->city_id=$model->newcity;
                if($addressmodel->save()){
                   $model->address_id=$addressmodel->address_id;
                   $model->store_id=1;
                   $model->active=1;
                   
                   if($model->save(false)){
                    return $this->redirect(['customer/index']);
                   }
                }
                
                
            }
        }
        
        return $this->render('_form',[
            'model'=>$model,
        ]); 
    }


    public function actionCreate(){
        $model=new Customer();
        $addressmodel=new Address();
        if($this->request->isPost){
            if($model->load($this->request->post())){
                $addressmodel->address=$model->newaddress;
                $addressmodel->district=$model->newdistrict;
                $addressmodel->postal_code=$model->postalcode;
                $addressmodel->phone=$model->newphone;
                $addressmodel->city_id=$model->newcity;
                if($addressmodel->save()){
                   $model->address_id=$addressmodel->address_id;
                   $model->store_id=1;
                   $model->active=1;
                   
                   if($model->save(false)){
                    return $this->redirect(['customer/index']);
                   }
                }
                
                
            }
        }
        return $this->render('create',[
            'model'=>$model,
        ]);
    }

    public function actionGetcountry(){
        $city_id=$_POST['city_id'];
        $city=City::find()->where(['city_id'=>$city_id])->all();
        $country=Country::find()->where(['country_id'=>$city[0]->country_id])->all();
        echo($country[0]->country);
    }
    protected function findModel($id){
        
        if(!empty($id)){

            $model= new CustomerSearch();
            $dataProvider=$model->search($id);
        }
        else{

            $dataProvider= array();
        }
            return $dataProvider;
    }
}

?>