<?php
namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Actor;

class ActorSearch extends Actor{


    public function search($params){
        $query=Actor::find();

        $dataProvider=new ActiveDataProvider([
            'query'=>$query,
        ]);

        $this->load($params);
        
        $query->andFilterWhere([
            'first_name'=>$this->first_name,
            'last_name'=>$this->last_name,
        ]);

        $query->andFilterWhere(['like','first_name',$this->first_name])
            ->andFilterWhere(['like','last_name',$this->last_name]);

        return $dataProvider;
    }
}


?>