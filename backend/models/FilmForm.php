<?php
namespace backend\models;

use Yii;
use yii\base\Model;


class FilmForm extends Model{
    public $filmname;
    public $actor_firstname;
    public $actor_lastname;
    public $discription;

    public function rules(){
        return [
            [['filmname','actor_firstname','actor_lastname','discription'],'required','message'=>'This field can not be blank'],
        ];
    }
    
    public function attributeLabels(){
        return [
            'filmname'=>'Movie Name',
            'actor_firstname'=>"Actor's First Name",
            'actor_lastname'=>"Actor's Last Name",
            'discription'=>'A little about movie',
        ];
    }
}

?>