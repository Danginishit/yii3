<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Customer;

/**
 * CustomerSearch represents the model behind the search form of `backend\models\Customer`.
 */
class CustomerSearch extends Customer
{
    public $name;
    public $address;
    public $dpcode;
    public $citycountry;
    public $phone;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'store_id', 'address_id', 'active'], 'integer'],
            [['first_name', 'last_name', 'email', 'create_date', 'last_update','name','address','dpcode','citycountry','phone'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find();
        $query->joinWith(['address','address.city','address.city.country']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if(!is_array($params)){
            $query->andFilterWhere(['like','customer_id',$params]);
        }
        $this->load($params);
       
        // echo '<pre>'; print_r($this->citycountry);exit();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       
        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andfilterWhere(['or',['like','first_name',$this->name],['like','last_name',$this->name]])
            ->andFilterWhere(['like', 'address.address',$this->address])
            ->andFilterWhere(['or',['like','address.postal_code',$this->dpcode],['like','address.district',$this->dpcode]])
            ->andfilterWhere(['or',['like','city.city',$this->citycountry],['like','country.country',$this->citycountry]])
            ->andFilterWhere(['like','address.phone',trim($this->phone)]);


        return $dataProvider;
    }
}
