<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "film_category".
 *
 * @property int $film_id
 * @property int $category_id
 * @property string $last_update
 */
class FilmCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'film_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['film_id', 'category_id'], 'required'],
            [['film_id', 'category_id'], 'integer'],
            [['last_update'], 'safe'],
            [['film_id', 'category_id'], 'unique', 'targetAttribute' => ['film_id', 'category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'film_id' => 'Film ID',
            'category_id' => 'Category ID',
            'last_update' => 'Last Update',
        ];
    }

    public function getCategoryname(){
        return $this->hasOne(Category::className(),[
            'category_id'=>'category_id'
        ]);
    }
}
