<?php

namespace backend\models;
use yii\db\ActiveRecord;

class Actor extends ActiveRecord{
    
    /* parameter declaration */

    
    /* table name */
    public static function tableName(){
        return 'actor';
    }

    /* rules declaration */
    public function rules(){
        return[
            [['first_name','last_name'],'required'],
            ['first_name','validatefirstname']
        ];
    }
    /* lable names*/
    public function attributeLables(){
        return [
            'first_name'=>'First Name',
            'last_name'=>'Last Name'
        ];
    }

    public function validatefirstname($attributes){
        if($this->first_name!=$this->last_name){

            $this->addError($attributes,"helo");
        }
    }
}


?>