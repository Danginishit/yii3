<?php

namespace backend\models;

use yii\db\ActiveRecord;
use backend\models\FilmActor;
use backend\models\Actor;

class Film extends ActiveRecord{

    public $searchcategory;
    public $searchactor;
    /* table name */
    public static function tableName(){
        return 'film';
    }

    public function rules(){
        return[
            [['title','description','release_year','searchcategory','searchactor','length','rating'],'required']
        ];
    }
    public function attributeLabels(){
        return[
            'title'=>'Movie Name',
            'description'=>'Movie Description',
            'release_year'=>'Year of release',
            'actor.actorname.first_name'=>'Full Name'
        ];
    }
    public function getActor(){
        return ($this->hasMany(FilmActor::className(),
        ['film_id'=>'film_id']));
    }
    public function getCategory(){
        return ($this->hasOne(FilmCategory::className(),
        ['film_id'=>'film_id']));
    }

    public static function getactorsfullnames($arr,$cnt){
        $fullname=null;
        if(isset($arr) && !empty($arr)){
            for($i=0;$i<$cnt;$i++){
                if($i==0){
                    $fullname=$arr[0]->actorname->first_name.' '.$arr[0]->actorname->last_name;
                }
                else{
                    $fullname=$fullname.','.$arr[$i]->actorname->first_name.' '.$arr[$i]->actorname->last_name;
                }
            }
        }
        return $fullname;
    }
  
}



?>