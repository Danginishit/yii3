<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Flim;

class FilmSearch extends Film{


    public function search($params){
        $query=Film::find();
        $query->joinWith(['actor','actor.actorname','category','category.categoryname']);
        $this->load($params);
        // echo '<pre>'; print_r($this->title);exit();
        $query->andFilterWhere([
            'title'=>$this->title,
            'description'=>$this->description,
            'release_year'=>$this->release_year,
            'length'=>$this->length,
            'rating'=>$this->rating,
            
        ]);
        $query->andFilterWhere(['like','category.name',$this->searchcategory])
        ->andFilterWhere(['or',['like','actor.first_name',$this->searchactor],['like','actor.last_name',$this->searchactor]])
        ->andFilterWhere(['like','title',$this->title]);
        $dataProvider=new ActiveDataProvider([
            'query'=>$query,
            'pagination' => [
                'pageSize' => 115,
            ],
        ]);

        return $dataProvider;
    }
}

?>