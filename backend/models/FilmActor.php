<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "film_actor".
 *
 * @property int $actor_id
 * @property int $film_id
 * @property string $last_update
 */
class FilmActor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'film_actor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['actor_id', 'film_id'], 'required'],
            [['actor_id', 'film_id'], 'integer'],
            [['last_update'], 'safe'],
            [['actor_id', 'film_id'], 'unique', 'targetAttribute' => ['actor_id', 'film_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'actor_id' => 'Actor ID',
            'film_id' => 'Film ID',
            'last_update' => 'Last Update',
        ];
    }
    public function getActorname(){
        return($this->hasOne(Actor::className(),
        ['actor_id'=>'actor_id']));
    }
}
