<?php

use yii\db\Migration;

class m220629_054124_create_table_actor extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%actor}}', [
            'actor_id' => $this->smallInteger()->unsigned()->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'first_name' => $this->string(45)->notNull(),
            'last_name' => $this->string(45)->notNull(),
            'last_update' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex('idx_actor_last_name', '{{%actor}}', 'last_name');
    }

    public function down()
    {
        $this->dropTable('{{%actor}}');
    }
}
